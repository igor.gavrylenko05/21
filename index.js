// First task

const word = prompt('Write the word!').trim();
if (word != null && word.length != 0) {
    if(word.length % 2 == 0) {
        alert(word[word.length / 2 - 1] + word[word.length / 2]);
    } else {
        alert(word[Math.floor(word.length / 2)]);
    }
} else if(word.length == 0){
    alert("Invalid value!");
} else {
    alert("Cancelled!");
} 

// Second task

let array = [
    {
          id: "701b29c3-b35d-4cf1-a5f6-8b12b29a5081",
          name: "Moore Hensley",
          email: "moorehensley@indexia.com",
          eyeColor: "blue",
          friends: ["Sharron Pace"],
          isActive: false,
          balance: 2811,
          skills: ["ipsum", "lorem"],
          gender: "male",
          age: 37,
        },
        {
          id: "7a3cbd18-57a1-4534-8e12-1caad921bda1",
          name: "Sharlene Bush",
          email: "sharlenebush@tubesys.com",
          eyeColor: "blue",
          friends: ["Briana Decker", "Sharron Pace"],
          isActive: true,
          balance: 3821,
          skills: ["tempor", "mollit", "commodo", "veniam", "laborum"],
          gender: "female",
          age: 34,
        },
        {
          id: "88beb2f3-e4c2-49f3-a0a0-ecf957a95af3",
          name: "Ross Vazquez",
          email: "rossvazquez@xinware.com",
          eyeColor: "green",
          friends: ["Marilyn Mcintosh", "Padilla Garrison", "Naomi Buckner"],
          isActive: false,
          balance: 3793,
          skills: ["nulla", "anim", "proident", "ipsum", "elit"],
          gender: "male",
          age: 24,
        },
        {
          id: "249b6175-5c30-44c6-b154-f120923736f5",
          name: "Elma Head",
          email: "elmahead@omatom.com",
          eyeColor: "green",
          friends: ["Goldie Gentry", "Aisha Tran"],
          isActive: true,
          balance: 2278,
          skills: ["adipisicing", "irure", "velit"],
          gender: "female",
          age: 21,
        },
        {
          id: "334f8cb3-eb04-45e6-abf4-4935dd439b70",
          name: "Carey Barr",
          email: "careybarr@nurali.com",
          eyeColor: "blue",
          friends: ["Jordan Sampson", "Eddie Strong"],
          isActive: true,
          balance: 3951,
          skills: ["ex", "culpa", "nostrud"],
          gender: "male",
          age: 27,
        }
    ];

function secondTaskPartOne(array) {
    let result = [];
    for(let elem of array) {
        for(let skill of elem.skills) {
            if(!result.includes(skill)){
                result.push(skill);
            }
        }
    }

    return result;
}

function secondTaskPartTwo(array) {
    let sum = 0;
    for(let elem of array) {
        if(elem.friends.length === 2 && elem.age < 30){
            sum += elem.balance;
        }
    }

    return sum;
}

console.log(secondTaskPartOne(array), secondTaskPartTwo(array));

// Third task

const arr = [1, 2, 3, 4, 5];
{
        // Rest operator
        const [first, ...rest] = arr;
        first // 1
        rest // [2,3,4,5]
        // Rest is used to contract what would have been a list of separated values (by comma) into an array variable

        // Spread operator
        const array2 = [first, ...rest];
        array2 // [1,2,3,4,5]
        // Spread is used to expand elements from an array into individual values
}

// Fourth task

const body = document.body;
const list = document.createElement('ul');
body.append(list);

for(let elem of array) {
    const listElem = document.createElement('li');
    listElem.style.cursor = 'pointer';
    listElem.textContent = elem.name;
    listElem.addEventListener('click', () => {
        alert(`Hello, ${listElem.textContent}`);
        // If we need just name, we can use split method to get name of the person
        // alert(`Hello, ${listElem.textContent.split(' ')[0]}`);
    })
    list.append(listElem);
}